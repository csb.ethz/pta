"""Common definitions used in the PTA package.
"""

import enkie

Q = enkie.Q
"""Type used for describing quantities.
"""
