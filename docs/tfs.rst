.. _tfs:

**********************************
Sampling thermodynamics and fluxes
**********************************

Thermodynamics and Flux Sampling (TFS) allows to sample steady state reaction energies,
standard reaction energies, metabolite concentrations and fluxes in a metabolic network. 

TFS consists in two main steps:

1. Sample reaction energies and orthants (sets of reaction directions in the model).
2. Sample standard reaction energies, metabolite concentrations and reaction fluxes
   conditioned on the sampled reaction energies and orthants.

Sampling reaction energies
==========================

The first step is to sample reaction energies and estimate the probability of the
orthants of the thermodynamic space:

.. code-block:: python
    :linenos:
    
    # Wrap a cobrapy model and the corresponding thermodynamic space in a single object.
    tfs_model = pta.TFSModel(model, thermodynamic_space)

    # Sample the thermodynamic space.
    result = pta.sample_drg(tfs_model)

The :code:`result` object contains the following fields:

* :code:`samples`:  Pandas data frame containing the samples of reaction energies.
* :code:`orthants`:  Pandas data frame containing the signs (-1 / +1) of the sampled
  orthants. An additional column :code:`weight` stores the weight of each orthant.
* :code:`psrf` Pandas series containing the Potential Scale Reduction Factor (PSRF) of
  each reaction energy. This is only used to assess convergence of the sampling
  procedure.

Note that the space of steady state reaction energies is generally non-convex and
disconnected, thus convergence can take a high number of steps. While PTA tries to
automatically choose a good number of steps, this may not be sufficient for challenging
models and you may get a warning that sampling did not converge. In this case you should
manually tell :code:`sample_drg()` to use more steps using the :code:`num_steps` option.

Sampling the conditionals
=========================

The remaining quantities can be sampled from the reaction energies:

.. code-block:: python
    :linenos:
    
    # Sample the natural logarithm of the metabolite concentrations. This function draws
    # one concentration sample for each reaction energy sample.
    log_conc = sample_log_conc_from_drg(thermodynamic_space, result.samples)

    # Sample the standard reaction energies. This function draws one standard reaction
    # energy sample for each reaction energy sample.
    drg0 = sample_drg0_from_drg(thermodynamic_space, result.samples)

    # Sample reaction fluxes. For each unique orthant represented by the reaction energy
    # samples this function draws a number of flux samples proportional to the weight of
    # the orthant.
    fluxes = sample_fluxes_from_drg(model, result.samples, result.orthants)

Each function returns a Pandas data frame containing the samples. For concentrations
and standard reaction energies, PTA can use an analytical expression of the conditional
probability, which makes sampling very fast. However, for fluxes we need to run uniform
sampling on each orthant separately, which can take a long time for medium-large models.
If sampling fluxes takes too long, pass fewer reaction energy samples to reduce the
number of orthants sampled.