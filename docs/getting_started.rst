.. _getting_started:

Getting started
===============

************
Requirements
************

In order to install PTA you need:

* Windows, Linux or OSX operating system.
* Python 3.8 or newer.
* One of the QP + MILP `solvers supported by CVXPY
  <https://www.cvxpy.org/tutorial/advanced/index.html#choosing-a-solver>`_. For models
  where all reaction directions are fixed, a QP solver is sufficient.
* *Optional (only for running the samplers)*: 
  
  * A compiler supporting C++ 17.
  * The Gurobi solver (free for academia). Make sure you install the `python bindings
    <https://support.gurobi.com/hc/en-us/articles/360044290292-How-do-I-install-Gurobi-for-Python->`_.
    On OSX systems you may need to manually set the environment variable :code:`GUROBI_HOME`
    to :code:`/Library/gurobi<version>/<platform>`, since the Gurobi installer does not do it
    automatically.
  
************
Installation
************

Once your system satisfies all the requirements, you can install PTA through the Python
Package Index:

.. code-block::

   pip install pta

Installing PTA without samplers
#################################

You can install PTA without compiling the samplers by defining the `PTA_NO_SAMPLERS`
environment variable:

.. code-block::

   PTA_NO_SAMPLERS=1 pip install pta             # Linux, OSX
   set "PTA_NO_SAMPLERS=1" && pip install pta    # Windows

Installing PTA this way does not require Gurobi and the C++ compiler, but the
functionalities will be limited to running PMO and model assessment methods. The sampler
will not work.

**************
Simple example
**************

This example shows a basic workflow with PTA. First it loads a model and creates its
thermodynamic space, then it runs PMO and TFS on it.

.. code-block:: python
    :linenos:

    import enkie
    import pta

    # Load a SBML model and set bounds on known rates (e.g. growth).
    model = pta.load_example_model("e_coli_core")
    model.reactions.BIOMASS_Ecoli_core_w_GAM.lower_bound = 0.5

    # Preprocess the model to make sure it is thermodynamically consistent.
    pta.prepare_for_pta(model)

    # Construct the thermodynamic space of the model.
    thermodynamic_space = pta.ThermodynamicSpace.from_cobrapy_model(
      model, 
      metabolites_namespace="bigg.metabolite",
      parameters=enkie.CompartmentParameters.load("e_coli")
    )

    # Run PMO on the model (by default it maximizes the probability of concentrations
    # and reaction energies).
    problem = pta.PmoProblem(model, thermodynamic_space)
    problem.solve()

    # Analyze the predicted concentrations and reaction energies, revealing potential
    # knowledge gaps and inaccuracies in the model.
    pta.QuantitativeAssessment(problem).summary()

    # Sample the thermodynamic space of the network.
    tfs_model = pta.TFSModel(model, thermodynamic_space, solver="GUROBI")
    sampling_result = pta.sample_drg(tfs_model)
    # sampling_result now contains samples of reaction energies as well as the
    # probability of each orthant.


*********************
Expanding the example
*********************


The next pages of this documentation provide more details about each step and how to
customize it for your work. You can also look at additional 
`examples <https://gitlab.com/csb.ethz/pta/-/tree/main/examples>`_.

* You can gain more control over the :ref:`preprocessing step <preprocessing>`.
* The thermodynamic space should be :ref:`constructed <thermodynamic_space>` using
  parameters (pH, ionic strength, ...) and metabolite concentrations representative of
  the system you are modeling.
* You can use different objectives for PMO and :ref:`access <pmo>` the predicted values
  directly.
* You can use the information :ref:`provided <assessment>` by
  :code:`QuantitativeAssessment` to curate your model. 
* TFS can be used to :ref:`characterize <tfs>` the feasible thermodynamic and flux
  spaces of the network. 
